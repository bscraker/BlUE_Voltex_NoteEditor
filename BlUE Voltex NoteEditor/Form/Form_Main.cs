﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;

namespace BlUE_Voltex_NoteEditor
{
    using Class;
    using FMOD;

    public partial class Form_Main : Form
    {
        Music   music;

        System  system          = null;
        Channel channel_music   = null;
        Channel channel_hit     = null;
        Sound   sound_music     = null;
        Sound   sound_hit       = null;

        bool noteEditable = false;

        Bitmap[] bufferBitmap;
        Graphics[] bufferGraphics;

        List<Note>[][] currentNotes;
        List<Speed>[] currentSpeeds;
        List<BPM>[] currentBPMs;

        int[] lastNoteIndex = {0, 0};
        int[] selectedNoteIndex = {-1, -1};
        int selectedSpeedIndex = -1;
        int selectedBPMIndex = -1;

        float playCurrentPosition = 0.0f;
        int playCurrentOrder = 0;
        float playLastBPM = 0.0f;

        String datas = "";
        String currentData = "";

        float[] led = {0.0f, 0.0f};

        public Form_Main()
        {
            InitializeComponent();

            base.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            base.SetStyle(ControlStyles.ResizeRedraw, true);

            bufferBitmap = new Bitmap[3];
            bufferBitmap[0] = new Bitmap(200, 600);
            bufferBitmap[1] = new Bitmap(200, 600);
            bufferBitmap[2] = new Bitmap(200, 600);
            bufferGraphics = new Graphics[3];
            bufferGraphics[0] = Graphics.FromImage(bufferBitmap[0]);
            bufferGraphics[1] = Graphics.FromImage(bufferBitmap[1]);
            bufferGraphics[2] = Graphics.FromImage(bufferBitmap[2]);

            currentNotes = new List<Note>[3][];
            currentNotes[0] = new List<Note>[2];
            currentNotes[1] = new List<Note>[2];
            currentNotes[2] = new List<Note>[2];
            currentNotes[0][0] = new List<Note>();
            currentNotes[0][1] = new List<Note>();
            currentNotes[1][0] = new List<Note>();
            currentNotes[1][1] = new List<Note>();
            currentNotes[2][0] = new List<Note>();
            currentNotes[2][1] = new List<Note>();
            currentSpeeds = new List<Speed>[3];
            currentSpeeds[0] = new List<Speed>();
            currentSpeeds[1] = new List<Speed>();
            currentSpeeds[2] = new List<Speed>();
            currentBPMs = new List<BPM>[3];
            currentBPMs[0] = new List<BPM>();
            currentBPMs[1] = new List<BPM>();
            currentBPMs[2] = new List<BPM>();

            FormBorderStyle = FormBorderStyle.FixedSingle;

            music = new Music();

            Factory.System_Create(out system);
            system.init(32, INITFLAGS.NORMAL, (IntPtr)null);

            EnableTabPage(tabPage_Information, false);
            EnableTabPage(tabPage_Note, false);

            timer_Main.Enabled = true;
        }

        ~Form_Main()
        {
            if (sound_music != null)
            {
                sound_music.release();
            }
            
            if (sound_hit != null)
            {
                sound_hit.release();
            }

            if (system != null)
            {
                system.close();
                system.release();
            }
        }

        private void EnableTabPage(TabPage tabPage, bool enable)
        {
            foreach (Control control in tabPage.Controls)
            {
                control.Enabled = enable;
            }
        }

        private void DrawNote()
        {
            for (int i = 0; i < 3; ++i)
			{
			    for (int j = 0; j < Music.MAX_NOTES; ++j)
			    {
                    int MAX = currentNotes[i][j].Count;

                    for (int k = 0; k < MAX; ++k)
                    {
                        if (currentNotes[i][j][k].longNote)
                        {
                            int index1 = i;
                            int index2 = k + 1;

                            if (k >= MAX - 1)
                            {
                                index2 = 0;

                                while (++index1 < 3)
                                {
                                    if (currentNotes[index1][j].Count > 0)
                                    {
                                        break;
                                    }
                                }
                            }

                            Note endNote = null;

                            if (index1 < 3)
                            {
                                endNote = currentNotes[index1][j][index2];
                            }

                            DrawLongNote(i, j, currentNotes[i][j][k], index1, endNote);
                        }

                        bool selected = false;

                        if (i == 1 &&
                            j == selectedNoteIndex[0] &&
                            k == selectedNoteIndex[1])
                        {
                            selected = true;
                        }

                        DrawNote(i, currentNotes[i][j][k], j, selected);
                    }
			    } 
			}
        }

        private void DrawNote(int graphicsIndex, Note note, int index, bool selected)
        {
            int barPerPage = 0;

            int.TryParse(textBox_BarPerPage.Text, out barPerPage);

            if (barPerPage <= 0 || barPerPage > 8)
            {
                return;
            }

            int bar_index = note.order % barPerPage;

            int x = (int)((float)(200 * 5 / 6) * note.positionNumerator / note.positionDenominator);
            int y = (int)((bar_index + (float)note.beatNumerator / note.beatDenominator) * 600 / barPerPage);

            Brush brush;
            
            if (index == 0)
            {
                if (selected)
                {
                    brush = Brushes.Blue;                    
                }
                else
                {
                    brush = Brushes.DodgerBlue;
                }
            }
            else
            {
                if (selected)
                {
                    brush = Brushes.Crimson;
                }
                else
                {
                    brush = Brushes.DeepPink;
                }
            }

            bufferGraphics[graphicsIndex].FillRectangle(brush, x, 600 - y - 3, 200 / 6, 7);
        }

        private void DrawLongNote(int graphicsIndex, int index, Note startNote, int endGraphicsIndex, Note endNote)
        {
            int barPerPage = 0;

            int.TryParse(textBox_BarPerPage.Text, out barPerPage);

            if (barPerPage <= 0 || barPerPage > 8)
            {
                return;
            }

            int x = (int)((float)(200 * 5 / 6) * startNote.positionNumerator / startNote.positionDenominator);
            int y = (int)((startNote.order % barPerPage + (float)startNote.beatNumerator / startNote.beatDenominator) *
                          600 / barPerPage);

            int endX = 0;
            int endY = 0;

            if (endNote != null)
            {                
                endX = (int)((float)(200 * 5 / 6) * endNote.positionNumerator / endNote.positionDenominator);
                endY = (int)((endNote.order % barPerPage + (float)endNote.beatNumerator / endNote.beatDenominator) *
                             600 / barPerPage);
            }

            Brush brush;

            if (index == 0)
            {
                brush = Brushes.LightBlue;
            }
            else
            {
                brush = Brushes.LightPink;
            }

            if (graphicsIndex == endGraphicsIndex)
            {
                bufferGraphics[graphicsIndex].FillRectangle(brush, x, 600 - 1 - endY, 200 / 6, endY - y);
            }
            else
            {
                bufferGraphics[graphicsIndex].FillRectangle(brush, x, 0, 200 / 6, 600 - 1 - y);

                for (int i = graphicsIndex + 1; i < 3 && i < endGraphicsIndex; ++i)
                {
                    if (i == endGraphicsIndex)
                    {
                        bufferGraphics[i].FillRectangle(brush, x, 600 - 1 - endY, 200 / 6, 600 - 1 - endY);
                    }
                    else
                    {
                        bufferGraphics[i].FillRectangle(brush, x, 0, 200 / 6, 600);
                    }
                }
            }

        }

        private void DrawSpeed()
        {
            for (int i = 0; i < 3; ++i)
            {
                int MAX = currentSpeeds[i].Count;

                for (int j = 0; j < MAX; ++j)
                {
                    bool selected = false;

                    if (i == 1 &&
                        j == selectedSpeedIndex)
                    {
                        selected = true;
                    }

                    DrawSpeed(i, currentSpeeds[i][j], selected);
                }
            }
        }

        private void DrawSpeed(int graphicsIndex, Speed speed, bool selected)
        {
            int barPerPage = 0;

            int.TryParse(textBox_BarPerPage.Text, out barPerPage);

            if (barPerPage <= 0 || barPerPage > 8)
            {
                return;
            }

            int bar_index = speed.order % barPerPage;

            int y = (int)((bar_index + (float)speed.beatNumerator / speed.beatDenominator) * 600 / barPerPage);

            Pen pen;
        
            if (selected)
            {
                pen = Pens.DarkMagenta;
            }
            else
            {
                pen = Pens.Magenta;
            }
            
            bufferGraphics[graphicsIndex].DrawLine(pen, 0, 600 - 1 - y, 200 - 1, 600 - 1 - y);
        }

        private void DrawBPM()
        {
            for (int i = 0; i < 3; ++i)
            {
                int MAX = currentBPMs[i].Count;

                for (int j = 0; j < MAX; ++j)
                {
                    bool selected = false;

                    if (i == 1 &&
                        j == selectedBPMIndex)
                    {
                        selected = true;
                    }

                    DrawBPM(i, currentBPMs[i][j], selected);
                }
            }
        }

        private void DrawBPM(int graphicsIndex, BPM bpm, bool selected)
        {
            int barPerPage = 0;

            int.TryParse(textBox_BarPerPage.Text, out barPerPage);

            if (barPerPage <= 0 || barPerPage > 8)
            {
                return;
            }

            int bar_index = bpm.order % barPerPage;

            int y = (int)((bar_index + (float)bpm.beatNumerator / bpm.beatDenominator) * 600 / barPerPage);

            Pen pen;

            if (selected)
            {
                pen = Pens.Green;
            }
            else
            {
                pen = Pens.Lime;
            }

            bufferGraphics[graphicsIndex].DrawLine(pen, 0, 600 - 1 - y, 200 - 1, 600 - 1 - y);
        }

        private void DrawPlayLine(int graphicsIndex)
        {
            int barPerPage = 0;

            int.TryParse(textBox_BarPerPage.Text, out barPerPage);

            if (barPerPage <= 0 || barPerPage > 8)
            {
                return;
            }

            if (channel_music == null)
            {
                return;
            }

            int position = 0;

            if (!button_Play.Enabled)
            {
                int bar = (int)(4 * 60000 / playLastBPM);

                position = (int)((((float)(playCurrentPosition % bar) / bar) +
                                     (playCurrentOrder % barPerPage)) * 600 / barPerPage);
            }

            bufferGraphics[graphicsIndex].FillRectangle(Brushes.LightGreen, 0, 600 - position, 200 - 1, position);

            if (currentData.Length > 0)
            {
                Data data = new Data(currentData);
            
                int[] x = {200 * data.knob[0] * 5 / 6 / 1023, 200 * data.knob[1] * 5 / 6 / 1023};

                bufferGraphics[graphicsIndex].FillRectangle(Brushes.Blue, x[0], 600 - position - 3, 200 / 6, 7);
                bufferGraphics[graphicsIndex].FillRectangle(Brushes.Red, x[1], 600 - position - 3, 200 / 6, 7);
            }
        }

        private void DrawCommon(int graphicsIndex)
        {
            bufferGraphics[graphicsIndex].DrawLine(Pens.Black, 200 - 1, 0, 200 - 1, 600 - 1);

            int barPerPage = 0;

            int.TryParse(textBox_BarPerPage.Text, out barPerPage);

            if (barPerPage <= 0 || barPerPage > 8)
            {
                return;
            }

            for (int i = 1; i < barPerPage; ++i)
            {
                int h = i * panel_Current.Height / barPerPage;
                bufferGraphics[graphicsIndex].DrawLine(Pens.Black, 0, h, 200 - 1, h);
            }
        }

        private void panel_Previous_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = Graphics.FromHwnd(panel_Previous.Handle);
            graphics.DrawImage(bufferBitmap[0], 0, 0);
        }

        private void panel_Current_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = Graphics.FromHwnd(panel_Current.Handle);
            graphics.DrawImage(bufferBitmap[1], 0, 0);
        }

        private void panel_Next_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = Graphics.FromHwnd(panel_Next.Handle);
            graphics.DrawImage(bufferBitmap[2], 0, 0);
        }

        private void button_Create_Click(object sender, EventArgs e)
        {
            textBox_SaveName.Text = textBox_SaveName.Text.Trim();
            textBox_ImageFile.Text = textBox_ImageFile.Text.Trim();
            textBox_MusicFile.Text = textBox_MusicFile.Text.Trim();
            textBox_HitFile.Text = textBox_HitFile.Text.Trim();

            if (textBox_SaveName.Text.Length == 0 ||
                textBox_ImageFile.Text.Length == 0 ||
                textBox_MusicFile.Text.Length == 0 ||
                textBox_HitFile.Text.Length == 0)
            {
                MessageBox.Show("Text Error");

                return;
            }

            bool result = music.Create(textBox_SaveName.Text, textBox_ImageFile.Text,
                                       textBox_MusicFile.Text, textBox_HitFile.Text);

            if (!result)
            {
                MessageBox.Show("Create Error");

                return;
            }

            State_Created();
        }

        private String GetFilePath(String filter)
        {
            openFileDialog_Load.Filter = filter;

            DialogResult result = openFileDialog_Load.ShowDialog();

            if (result == DialogResult.OK)
            {
                return openFileDialog_Load.FileName;
            }

            return "";
        }

        private void textBox_ImageFile_MouseClick(object sender, MouseEventArgs e)
        {
            textBox_ImageFile.Text = GetFilePath("PNG (*.png)|*.png");
        }

        private void textBox_MusicFile_MouseClick(object sender, MouseEventArgs e)
        {
            textBox_MusicFile.Text = GetFilePath("MP3 (*.mp3)|*.mp3");
        }

        private void ReLoadSet()
        {
            textBox_Name.Text = music.information.name;
            textBox_Artist.Text = music.information.artist;
            textBox_StartBPM.Text = music.information.startBPM.ToString();
            textBox_StartTime.Text = music.information.startTime.ToString();
            textBox_PreviewStartTime.Text = music.information.previewStartTime.ToString();
            textBox_PreviewPlayTime.Text = music.information.previewPlayTime.ToString();
            textBox_MusicVolume.Text = music.information.musicVolume.ToString();
            textBox_HitVolume.Text = music.information.hitVolume.ToString();
            textBox_HardDifficulty.Text = music.information.hardDifficulty.ToString();
            textBox_NormalDifficulty.Text = music.information.normalDifficulty.ToString();
            textBox_EasyDifficulty.Text = music.information.easyDifficulty.ToString();
        }

        private void button_ReLoad_Click(object sender, EventArgs e)
        {
            ReLoadSet();
        }

        private bool SaveSet()
        {
            textBox_Name.Text = textBox_Name.Text.Trim();
            textBox_Artist.Text = textBox_Artist.Text.Trim();
            textBox_StartBPM.Text = textBox_StartBPM.Text.Trim();
            textBox_StartTime.Text = textBox_StartTime.Text.Trim();
            textBox_PreviewStartTime.Text = textBox_PreviewStartTime.Text.Trim();
            textBox_PreviewPlayTime.Text = textBox_PreviewPlayTime.Text.Trim();
            textBox_MusicVolume.Text = textBox_MusicVolume.Text.Trim();
            textBox_HitVolume.Text = textBox_HitVolume.Text.Trim();
            textBox_HardDifficulty.Text = textBox_HardDifficulty.Text.Trim();
            textBox_NormalDifficulty.Text = textBox_NormalDifficulty.Text.Trim();
            textBox_EasyDifficulty.Text = textBox_EasyDifficulty.Text.Trim();

            int BPM = 0;
            int startTime = 0;
            int previewStartTime = 0;
            int previewPlayTime = 0;
            float musicVolume = 1.0f;
            float hitVolume = 1.0f;
            int hardDifficulty = 0;
            int normalDifficulty = 0;
            int easyDifficulty = 0;

            int.TryParse(textBox_StartBPM.Text, out BPM);
            int.TryParse(textBox_StartTime.Text, out startTime);
            int.TryParse(textBox_PreviewStartTime.Text, out previewStartTime);
            int.TryParse(textBox_PreviewPlayTime.Text, out previewPlayTime);
            float.TryParse(textBox_MusicVolume.Text, out musicVolume);
            float.TryParse(textBox_HitVolume.Text, out hitVolume);
            int.TryParse(textBox_HardDifficulty.Text, out hardDifficulty);
            int.TryParse(textBox_NormalDifficulty.Text, out normalDifficulty);
            int.TryParse(textBox_EasyDifficulty.Text, out easyDifficulty);

            textBox_StartBPM.Text = BPM.ToString();
            textBox_StartTime.Text = startTime.ToString();
            textBox_PreviewStartTime.Text = previewStartTime.ToString();
            textBox_PreviewPlayTime.Text = previewPlayTime.ToString();
            textBox_MusicVolume.Text = musicVolume.ToString();
            textBox_HitVolume.Text = hitVolume.ToString();
            textBox_HardDifficulty.Text = hardDifficulty.ToString();
            textBox_NormalDifficulty.Text = normalDifficulty.ToString();
            textBox_EasyDifficulty.Text = easyDifficulty.ToString();

            if (textBox_Name.Text.Length == 0 ||
                textBox_Artist.Text.Length == 0 ||
                BPM <= 0 ||
                startTime < 0 ||
                previewStartTime < 0 ||
                previewPlayTime <= 0 ||
                musicVolume < 0.0f || musicVolume > 1.0f ||
                hitVolume < 0.0f || hitVolume > 1.0f ||
                hardDifficulty < 1 || normalDifficulty < 1 || easyDifficulty < 1)
            {
                MessageBox.Show("Text Error");

                return false;
            }

            music.SaveInformation(textBox_Name.Text, textBox_Artist.Text, textBox_StartBPM.Text, textBox_StartTime.Text,
                                  textBox_PreviewStartTime.Text, textBox_PreviewPlayTime.Text,
                                  textBox_MusicVolume.Text, textBox_HitVolume.Text,
                                  textBox_HardDifficulty.Text,
                                  textBox_NormalDifficulty.Text,
                                  textBox_EasyDifficulty.Text);

            music.SaveNote();

            return true;
        }

        private void button_Set_Click(object sender, EventArgs e)
        {
            if (SaveSet())
            {
                State_Set();
            }
        }

        private void button_Load_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog_Load.ShowDialog();

            if (result == DialogResult.OK)
            {
                Music newMusic = new Music();

                int loadResult = newMusic.Load(folderBrowserDialog_Load.SelectedPath);

                if (loadResult == 0)
                {
                    MessageBox.Show("Not Music");
                }

                if (loadResult >= 1)
                {
                    music = newMusic;
                    State_Created();
                }

                if (loadResult >= 2)
                {
                    ReLoadSet();
                    State_Set();
                }
            }
        }

        private void State_Created()
        {
            textBox_SaveName.Enabled = false;
            textBox_ImageFile.Enabled = false;
            textBox_MusicFile.Enabled = false;
            textBox_HitFile.Enabled = false;
            button_Create.Enabled = false;
            button_Save.Enabled = true;
            EnableTabPage(tabPage_Information, true);
        }

        private void CountPage()
        {
            int barPerPage = 0;

            int.TryParse(textBox_BarPerPage.Text, out barPerPage);

            if (barPerPage <= 0 || barPerPage > 8)
            {
                textBox_BarPerPage.Text = "8";
                barPerPage = 8;
            }

            uint length = 0;
            sound_music.getLength(out length, TIMEUNIT.MS);

            long page = (long)((length - music.information.startTime) *
                               music.information.startBPM / barPerPage / 4 / 60000) + 1;
            label_Page.Text = page.ToString();

            hScrollBar_Page.Maximum = (int)page * 10;
        }

        private void State_Set()
        {
            if (sound_music != null)
            {
                sound_music.release();
            }

            if (sound_hit != null)
            {
                sound_hit.release();
            }

            system.createSound(music.savePath + Music.FILE_MUSIC, MODE.DEFAULT, out sound_music);
            system.createSound(music.savePath + Music.FILE_HIT, MODE.DEFAULT, out sound_hit);

            system.playSound(sound_music, null, true, out channel_music);
            system.playSound(sound_hit, null, true, out channel_hit);
            
            CountPage();
            PageChanged();

            EnableTabPage(tabPage_Note, true);

            noteEditable = true;
        }

        private void button_New_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Sure?", "New", MessageBoxButtons.YesNo);

            if (result != DialogResult.Yes)
            {
                return;
            }

            music = new Music();

            textBox_SaveName.Text = "";
            textBox_ImageFile.Text = "";
            textBox_MusicFile.Text = "";
            textBox_HitFile.Text = "";

            textBox_SaveName.Enabled = true;
            textBox_ImageFile.Enabled = true;
            textBox_MusicFile.Enabled = true;
            textBox_HitFile.Enabled = true;

            button_Create.Enabled = true;
            button_Save.Enabled = false;
            button_InGamePlay.Enabled = false;

            ReLoadSet();

            EnableTabPage(tabPage_Information, false);
            EnableTabPage(tabPage_Note, false);

            noteEditable = false;
        }

        private void button_Save_Click(object sender, EventArgs e)
        {
            if (SaveSet())
            {
                music.SaveNote();
            }
        }

        private void button_InGamePlay_Click(object sender, EventArgs e)
        {

        }

        private void AddNote(int index)
        {
            if (textBox_Order_New.Text.Length == 0 ||
                textBox_BeatNumerator_New.Text.Length == 0 ||
                textBox_BeatDenominator_New.Text.Length == 0 ||
                textBox_PositionNumerator_New.Text.Length == 0 ||
                textBox_PositionDenominator_New.Text.Length == 0)
            {
                MessageBox.Show("error");
                return;
            }

            int order = 0;
            int beatNumerator = 0;
            int beatDenominator = 0;
            int positionNumerator = 0;
            int positionDenominator = 0;

            int.TryParse(textBox_Order_New.Text, out order);
            int.TryParse(textBox_BeatNumerator_New.Text, out beatNumerator);
            int.TryParse(textBox_BeatDenominator_New.Text, out beatDenominator);
            int.TryParse(textBox_PositionNumerator_New.Text, out positionNumerator);
            int.TryParse(textBox_PositionDenominator_New.Text, out positionDenominator);

            if (order < 0 ||
                beatNumerator < 0 ||
                beatDenominator < 1 ||
                positionNumerator < 0 ||
                positionDenominator < 1)
            {
                MessageBox.Show("error");
                return;
            }

            Note note = new Note();
            note.order = order;
            note.beatNumerator = beatNumerator;
            note.beatDenominator = beatDenominator;
            note.positionNumerator = positionNumerator;
            note.positionDenominator = positionDenominator;

            bool result = music.AddNote(hScrollBar_Difficulty.Value, index, note);

            if (!result)
            {
                MessageBox.Show("error");
            }
            else
            {
                PageChanged();
            }
        }

        private void AddLongNote()
        {
            if (selectedNoteIndex[0] == -1 ||
                selectedNoteIndex[1] == -1)
            {
                MessageBox.Show("error");
                return;
            }

            bool result = music.AddLongNote(hScrollBar_Difficulty.Value, selectedNoteIndex[0],
                                            currentNotes[1][selectedNoteIndex[0]][selectedNoteIndex[1]]);

            if (!result)
            {
                MessageBox.Show("error");
            }
            else
            {
                PageChanged();
            }
        }

        private void AddSpeed()
        {
            if (textBox_Order_New.Text.Length == 0 ||
                textBox_BeatNumerator_New.Text.Length == 0 ||
                textBox_BeatDenominator_New.Text.Length == 0 ||
                textBox_Speed_New.Text.Length == 0)
            {
                MessageBox.Show("error");
                return;
            }

            int order = 0;
            int beatNumerator = 0;
            int beatDenominator = 0;
            float _speed = 0;

            int.TryParse(textBox_Order_New.Text, out order);
            int.TryParse(textBox_BeatNumerator_New.Text, out beatNumerator);
            int.TryParse(textBox_BeatDenominator_New.Text, out beatDenominator);
            float.TryParse(textBox_Speed_New.Text, out _speed);

            if (order < 0 ||
                beatNumerator < 0 ||
                beatDenominator < 1 ||
                _speed < 0.0f)
            {
                MessageBox.Show("error");
                return;
            }

            Speed speed = new Speed();
            speed.order = order;
            speed.beatNumerator = beatNumerator;
            speed.beatDenominator = beatDenominator;
            speed.speed = _speed;

            bool result = music.AddSpeed(hScrollBar_Difficulty.Value, speed);

            if (!result)
            {
                MessageBox.Show("error");
            }
            else
            {
                PageChanged();
            }
        }

        private void AddBPM()
        {
            if (textBox_Order_New.Text.Length == 0 ||
                textBox_BeatNumerator_New.Text.Length == 0 ||
                textBox_BeatDenominator_New.Text.Length == 0 ||
                textBox_BPM_New.Text.Length == 0)
            {
                MessageBox.Show("error");
                return;
            }

            int order = 0;
            int beatNumerator = 0;
            int beatDenominator = 0;
            int _bpm = 0;

            int.TryParse(textBox_Order_New.Text, out order);
            int.TryParse(textBox_BeatNumerator_New.Text, out beatNumerator);
            int.TryParse(textBox_BeatDenominator_New.Text, out beatDenominator);
            int.TryParse(textBox_BPM_New.Text, out _bpm);

            if (order < 0 ||
                beatNumerator < 0 ||
                beatDenominator < 1 ||
                _bpm <= 0)
            {
                MessageBox.Show("error");
                return;
            }

            BPM bpm = new BPM();
            bpm.order = order;
            bpm.beatNumerator = beatNumerator;
            bpm.beatDenominator = beatDenominator;
            bpm.bpm = _bpm;

            bool result = music.AddBPM(hScrollBar_Difficulty.Value, bpm);

            if (!result)
            {
                MessageBox.Show("error");
            }
            else
            {
                PageChanged();
            }
        }

        private void AddDragNote()
        {
            if (selectedNoteIndex[0] == -1 ||
                selectedNoteIndex[1] == -1 ||
                textBox_BeatDenominator_New.Text.Length == 0)
            {
                MessageBox.Show("error");
                return;
            }

            int beatDenominator = 0;

            int.TryParse(textBox_BeatDenominator_New.Text, out beatDenominator);

            if (beatDenominator < 1)
            {
                MessageBox.Show("error");
                return;
            }

            int type = 0;

            if (radioButton_LowToHigh.Checked)
            {
                type = 1;
            }
            else if (radioButton_HighToLow.Checked)
            {
                type = 2;
            }

            bool result = music.AddDragNote(hScrollBar_Difficulty.Value, selectedNoteIndex[0],
                                            currentNotes[1][selectedNoteIndex[0]][selectedNoteIndex[1]],
                                            beatDenominator, type);

            if (!result)
            {
                MessageBox.Show("error");
            }
            else
            {
                PageChanged();
            }
        }

        private void button_LNote_Click(object sender, EventArgs e)
        {
            AddNote(0);
        }

        private void button_RNote_Click(object sender, EventArgs e)
        {
            AddNote(1);
        }

        private void button_Speed_Click(object sender, EventArgs e)
        {
            AddSpeed();
        }

        private void button_LongNote_Click(object sender, EventArgs e)
        {
            AddLongNote();
        }

        private void button_DragNote_Click(object sender, EventArgs e)
        {
            AddDragNote();
        }

        private void button_Play_Click(object sender, EventArgs e)
        {            
            int barPerPage = 0;
            int.TryParse(textBox_BarPerPage.Text, out barPerPage);

            int currentOrder = (hScrollBar_Page.Value - 1) * barPerPage;

            float position = music.information.startTime;

            int lastOrder = 0;
            float lastBPM = music.information.startBPM;

            int bpmMax = music.bpms[hScrollBar_Difficulty.Value].Count;

            for (int i = 0; i < bpmMax; ++i)
            {
                BPM bpm = music.bpms[hScrollBar_Difficulty.Value][i];

                if (bpm.order < currentOrder)
                {
                    float orderSize = (bpm.order - lastOrder) + ((float)bpm.beatNumerator / bpm.beatDenominator);

                    position += orderSize * (4 * 60000 / lastBPM);

                    lastOrder = bpm.order + 1;
                    lastBPM = bpm.bpm;
                }
                else
                {
                    break;
                }
            }

            position += (currentOrder - lastOrder) * (4 * 60000 / lastBPM);
            
            if (channel_music != null)
            { 
                channel_music.stop(); 
            }

            lastNoteIndex[0] = 0;
            lastNoteIndex[1] = 0;
            
            system.playSound(sound_hit, null, true, out channel_hit);
            system.playSound(sound_music, null, true, out channel_music);

            channel_music.setPosition((uint)position, TIMEUNIT.MS);

            channel_hit.setVolume(music.information.hitVolume);
            channel_music.setVolume(music.information.musicVolume);

            channel_music.setPaused(false);

            EnableTabPage(tabPage_Note, false);
            button_Stop.Enabled = true;
        }

        private void button_Stop_Click(object sender, EventArgs e)
        {
            if (channel_music != null)
            {
                channel_music.stop();
            }

            if (button_Play.Enabled)
            {
                textBox_Page.Text = "1";
            }

            EnableTabPage(tabPage_Note, true);            
        }

        private void hScrollBar_Difficulty_Scroll(object sender, ScrollEventArgs e)
        {
            PageChanged();
        }

        private void hScrollBar_Page_Scroll(object sender, ScrollEventArgs e)
        {
            if (textBox_Page.Text != hScrollBar_Page.Value.ToString())
            {
                textBox_Page.Text = hScrollBar_Page.Value.ToString();
                PageChanged();
            }
        }

        private void textBox_BarPerPage_TextChanged(object sender, EventArgs e)
        {
            CountPage();
            textBox_Page_TextChanged(sender, e);
            PageChanged();
            Drawing();
        }

        private void SelectedNoteChanged(int index1, int index2)
        {
            selectedNoteIndex[0] = index1;
            selectedNoteIndex[1] = index2;

            if (index1 == -1 ||
                index2 == -1)
            {
                return;
            }

            Note note = currentNotes[1][index1][index2];

            textBox_Order_Selected.Text = note.order.ToString();
            textBox_BeatNumerator_Selected.Text = note.beatNumerator.ToString();
            textBox_BeatDenominator_Selected.Text = note.beatDenominator.ToString();
            textBox_PositionNumerator_Selected.Text = note.positionNumerator.ToString();
            textBox_PositionDenominator_Selected.Text = note.positionDenominator.ToString();
            
            textBox_Order_New.Text = note.order.ToString();
            textBox_BeatNumerator_New.Text = note.beatNumerator.ToString();
            textBox_BeatDenominator_New.Text = note.beatDenominator.ToString();
            textBox_PositionNumerator_New.Text = note.positionNumerator.ToString();
            textBox_PositionDenominator_New.Text = note.positionDenominator.ToString();
        }

        private void SelectedSpeedChanged(int index)
        {
            selectedSpeedIndex = index;
           
            if (index == -1)
            {
                return;
            }

            Speed speed = currentSpeeds[1][index];

            textBox_Order_Selected.Text = speed.order.ToString();
            textBox_BeatNumerator_Selected.Text = speed.beatNumerator.ToString();
            textBox_BeatDenominator_Selected.Text = speed.beatDenominator.ToString();
            textBox_Speed_Selected.Text = speed.speed.ToString();

            textBox_Order_New.Text = speed.order.ToString();
            textBox_BeatNumerator_New.Text = speed.beatNumerator.ToString();
            textBox_BeatDenominator_New.Text = speed.beatDenominator.ToString();
            textBox_Speed_New.Text = speed.speed.ToString();
        }

        private void SelectedBPMChanged(int index)
        {
            selectedBPMIndex = index;

            if (index == -1)
            {
                return;
            }

            BPM bpm = currentBPMs[1][index];

            textBox_Order_Selected.Text = bpm.order.ToString();
            textBox_BeatNumerator_Selected.Text = bpm.beatNumerator.ToString();
            textBox_BeatDenominator_Selected.Text = bpm.beatDenominator.ToString();
            textBox_BPM_Selected.Text = bpm.bpm.ToString();

            textBox_Order_New.Text = bpm.order.ToString();
            textBox_BeatNumerator_New.Text = bpm.beatNumerator.ToString();
            textBox_BeatDenominator_New.Text = bpm.beatDenominator.ToString();
            textBox_BPM_New.Text = bpm.bpm.ToString();
        }

        private void PageChanged()
        {
            int page = 0;
            int.TryParse(textBox_Page.Text, out page);

            if (page == 0)
            {
                return;
            }

            --page;

            int barPerPage = 0;

            int.TryParse(textBox_BarPerPage.Text, out barPerPage);

            if (barPerPage <= 0 || barPerPage > 8)
            {
                return;
            }

            currentNotes[0][0].Clear();
            currentNotes[0][1].Clear();
            currentNotes[1][0].Clear();
            currentNotes[1][1].Clear();
            currentNotes[2][0].Clear();
            currentNotes[2][1].Clear();
            currentSpeeds[0].Clear();
            currentSpeeds[1].Clear();
            currentSpeeds[2].Clear();
            currentBPMs[0].Clear();
            currentBPMs[1].Clear();
            currentBPMs[2].Clear();

            int currentOrderStart = page * barPerPage;

            int difficulty = hScrollBar_Difficulty.Value;

            for (int i = 0; i < Music.MAX_NOTES; ++i)
            {
                int noteMAX = music.notes[difficulty][i].Count;

                for (int j = 0; j < noteMAX; ++j)
                {
                    Note note = music.notes[difficulty][i][j];

                    if (note.order < currentOrderStart - barPerPage)
                    {
                        continue;
                    }
                    else if (note.order >= currentOrderStart - barPerPage &&
                             note.order < currentOrderStart)
                    {
                        currentNotes[0][i].Add(note);
                    }
                    else if (note.order >= currentOrderStart &&
                             note.order < currentOrderStart + barPerPage)
                    {
                        currentNotes[1][i].Add(note);
                    }
                    else if (note.order >= currentOrderStart + barPerPage &&
                             note.order < currentOrderStart + (barPerPage * 2))
                    {
                        currentNotes[2][i].Add(note);
                    }
                    else if (note.order >= currentOrderStart + (barPerPage * 2))
                    {
                        break;
                    }
                }
            }

            int speedMAX = music.speeds[difficulty].Count;

            for (int i = 0; i < speedMAX; ++i)
            {
                Speed speed = music.speeds[difficulty][i];

                if (speed.order < currentOrderStart - barPerPage)
                {
                    continue;
                }
                else if (speed.order >= currentOrderStart - barPerPage &&
                         speed.order < currentOrderStart)
                {
                    currentSpeeds[0].Add(speed);
                }
                else if (speed.order >= currentOrderStart &&
                         speed.order < currentOrderStart + barPerPage)
                {
                    currentSpeeds[1].Add(speed);
                }
                else if (speed.order >= currentOrderStart + barPerPage &&
                         speed.order < currentOrderStart + (barPerPage * 2))
                {
                    currentSpeeds[2].Add(speed);
                }
                else if (speed.order >= currentOrderStart + (barPerPage * 2))
                {
                    break;
                }
            }

            int bpmMAX = music.bpms[difficulty].Count;

            for (int i = 0; i < bpmMAX; ++i)
            {
                BPM bpm = music.bpms[difficulty][i];

                if (bpm.order < currentOrderStart - barPerPage)
                {
                    continue;
                }
                else if (bpm.order >= currentOrderStart - barPerPage &&
                         bpm.order < currentOrderStart)
                {
                    currentBPMs[0].Add(bpm);
                }
                else if (bpm.order >= currentOrderStart &&
                         bpm.order < currentOrderStart + barPerPage)
                {
                    currentBPMs[1].Add(bpm);
                }
                else if (bpm.order >= currentOrderStart + barPerPage &&
                         bpm.order < currentOrderStart + (barPerPage * 2))
                {
                    currentBPMs[2].Add(bpm);
                }
                else if (bpm.order >= currentOrderStart + (barPerPage * 2))
                {
                    break;
                }
            }

            lastNoteIndex[0] = 0;
            lastNoteIndex[1] = 0;

            SelectedNoteChanged(-1, -1);
            SelectedSpeedChanged(-1);
            SelectedBPMChanged(-1);
        }

        private void textBox_Page_TextChanged(object sender, EventArgs e)
        {
            int value = 0;
            int.TryParse(textBox_Page.Text, out value);

            if (value < hScrollBar_Page.Minimum)
            {
                value = hScrollBar_Page.Minimum;
                textBox_Page.Text = value.ToString();
            }
            else if (value > hScrollBar_Page.Maximum)
            {
                //value = hScrollBar_Page.Maximum;
                //textBox_Page.Text = value.ToString();
            }

            if (hScrollBar_Page.Value != value)
            {
                if (value >= hScrollBar_Page.Minimum && value <= hScrollBar_Page.Maximum)
                {
                    hScrollBar_Page.Value = value;
                }

                PageChanged();
            }
        }

        private void Drawing()
        {
            bufferGraphics[0].Clear(Color.LightGray);
            bufferGraphics[1].Clear(Color.White);
            bufferGraphics[2].Clear(Color.LightGray);

            if (noteEditable)
            {
                DrawPlayLine(1);
            }

            DrawCommon(0);
            DrawCommon(1);
            DrawCommon(2);

            if (noteEditable)
            {
                DrawBPM();
                DrawSpeed();
                DrawNote();
            }

            panel_Previous_Paint(null, null);
            panel_Current_Paint(null, null);
            panel_Next_Paint(null, null);
        }

        private void UpdateState()
        {
            if (!noteEditable || button_Play.Enabled)
            {
                return;
            }

            int barPerPage = 0;

            int.TryParse(textBox_BarPerPage.Text, out barPerPage);

            if (barPerPage <= 0 || barPerPage > 8)
            {
                return;
            }

            if (channel_music == null)
            {
                return;
            }

            uint _position = 0;

            channel_music.getPosition(out _position, TIMEUNIT.MS);

            if (_position == 0)
            {
                button_Stop_Click(null, null);
            }
            else
            {
                float position = _position - music.information.startTime;

                int currentOrder = 0;

                int lastOrder = 0;
                float lastBPM = music.information.startBPM;

                int bpmMax = music.bpms[hScrollBar_Difficulty.Value].Count;

                for (int i = 0; i < bpmMax; ++i)
                {
                    BPM bpm = music.bpms[hScrollBar_Difficulty.Value][i];

                    float orderSize = (bpm.order - lastOrder) + ((float)bpm.beatNumerator / bpm.beatDenominator);
                    float orderTime = orderSize * (4 * 60000 / lastBPM);

                    if (position >= orderTime)
                    {
                        currentOrder += bpm.order + 1 - lastOrder;
                        position -= orderTime;

                        lastOrder = bpm.order + 1;
                        lastBPM = bpm.bpm;
                    }
                    else
                    {
                        break;
                    }
                }

                int order = (int)(position / (4 * 60000 / lastBPM));

                currentOrder += order;
                position -= order * (4 * 60000 / lastBPM);

                playCurrentPosition = position;
                playCurrentOrder = currentOrder;
                playLastBPM = lastBPM;

                int page = (currentOrder / barPerPage) + 1;

                if (textBox_Page.Text != page.ToString())
                {
                    textBox_Page.Text = page.ToString();
                }

                for (int i = 0; i < 2; ++i)
                {
                    if (lastNoteIndex[i] > currentNotes[1][i].Count - 1)
                    {
                        continue;
                    }

                    Note note = currentNotes[1][i][lastNoteIndex[i]];

                    int note_position = (int)(note.beatNumerator * 4 * 60000 / lastBPM / note.beatDenominator);

                    if ((currentOrder == note.order && position >= note_position) ||
                        currentOrder > note.order)
                    {
                        SelectedNoteChanged(i, lastNoteIndex[i]);

                        system.playSound(sound_hit, null, true, out channel_hit);
                        channel_hit.setVolume(music.information.hitVolume);
                        channel_hit.setPaused(false);

                        ++lastNoteIndex[i];

                        led[i] = 1.0f;
                    }
                }

                if (serialPort_Main.IsOpen)
                {
                    serialPort_Main.Write("!" + 
                                          "0 " + 
                                          ((int)(63.0f * led[0])).ToString() + " " +
                                          ((int)(255.0f * led[0])).ToString() + " " +
                                          ((int)(255.0f * led[1])).ToString() + " " +
                                          "0 " +
                                          ((int)(63.0f * led[1])).ToString() + " " +
                                          "|");

                    led[0] *= (1000 - lastBPM) / 1000;
                    led[1] *= (1000 - lastBPM) / 1000;
                }
            }
        }

        private void timer_Main_Tick(object sender, EventArgs e)
        {
            system.update();

            UpdateState();
            
            Drawing();
        }

        private void textBox_Order_New_TextChanged(object sender, EventArgs e)
        {
            int value = -1;
            int.TryParse(textBox_Order_New.Text, out value);
            textBox_Order_New.Text = value.ToString();
        }

        private void textBox_BeatNumerator_New_TextChanged(object sender, EventArgs e)
        {
            int value = -1;
            int.TryParse(textBox_BeatNumerator_New.Text, out value);
            textBox_BeatNumerator_New.Text = value.ToString();
        }

        private void textBox_BeatDenominator_New_TextChanged(object sender, EventArgs e)
        {
            int value = -1;
            int.TryParse(textBox_BeatDenominator_New.Text, out value);

            if (value < 1)
            {
                value = 1;
            }

            textBox_BeatDenominator_New.Text = value.ToString();
        }

        private void textBox_PositionNumerator_New_TextChanged(object sender, EventArgs e)
        {
            int value = -1;
            int.TryParse(textBox_PositionNumerator_New.Text, out value);
            textBox_PositionNumerator_New.Text = value.ToString();
        }

        private void textBox_PositionDenominator_New_TextChanged(object sender, EventArgs e)
        {
            int value = -1;
            int.TryParse(textBox_PositionDenominator_New.Text, out value);

            if (value < 1)
            {
                value = 1;
            }
         
            textBox_PositionDenominator_New.Text = value.ToString();
        }

        private void textBox_Speed_New_TextChanged(object sender, EventArgs e)
        {
            float value = -1.0f;
            float.TryParse(textBox_Speed_New.Text, out value);

            if (value < 0.0f)
            {
                value = 0.0f;
            }

            textBox_Speed_New.Text = value.ToString();
        }

        private void panel_Current_MouseClick(object sender, MouseEventArgs e)
        {
            if (!button_Play.Enabled)
            {
                return;
            }
            
            int barPerPage = 0;

            int.TryParse(textBox_BarPerPage.Text, out barPerPage);

            if (barPerPage <= 0 || barPerPage > 8)
            {
                return;
            }
            
            double mouse_x = e.X;
            double mouse_y = 600.0 - e.Y;

            int[] noteIndex = new int[2] {-1, -1};
            double noteDistanse = -1.0;

            for (int i = 0; i < 2; ++i)
            {
                int noteMAX = currentNotes[1][i].Count;

                for (int j = 0; j < noteMAX; ++j)
                {
                    Note note = currentNotes[1][i][j];

                    double note_x = (200.0 * 5 / 6) * note.positionNumerator / note.positionDenominator +
                                    200.0 / 6 / 2;
                    double note_y = 600.0 *
                                    (note.order % barPerPage + (double)note.beatNumerator / note.beatDenominator) /
                                    barPerPage;

                    double _distanse = Math.Pow(mouse_x - note_x, 2) + Math.Pow(mouse_y - note_y, 2);

                    if (noteDistanse < 0 || noteDistanse > _distanse)
                    {
                        noteDistanse = _distanse;
                        noteIndex[0] = i;
                        noteIndex[1] = j;
                    }
                }
            }

            int speedIndex = -1;
            double speedDistanse = -1.0;

            int speedMAX = currentSpeeds[1].Count;

            for (int i = 0; i < speedMAX; ++i)
            {
                Speed speed = currentSpeeds[1][i];

                double speed_x = 200.0 / 2;
                double speed_y = 600.0 *
                                 (speed.order % barPerPage + (double)speed.beatNumerator / speed.beatDenominator) /
                                 barPerPage;

                double _distanse = Math.Pow(mouse_x - speed_x, 2) + Math.Pow(mouse_y - speed_y, 2);

                if (speedDistanse < 0 || speedDistanse > _distanse)
                {
                    speedDistanse = _distanse;
                    speedIndex = i;
                }
            }

            int bpmIndex = -1;
            double bpmDistanse = -1.0;

            int bpmMAX = currentBPMs[1].Count;

            for (int i = 0; i < bpmMAX; ++i)
            {
                BPM bpm = currentBPMs[1][i];

                double bpm_x = 200.0 / 2;
                double bpm_y = 600.0 *
                                 (bpm.order % barPerPage + (double)bpm.beatNumerator / bpm.beatDenominator) /
                                 barPerPage;

                double _distanse = Math.Pow(mouse_x - bpm_x, 2) + Math.Pow(mouse_y - bpm_y, 2);

                if (bpmDistanse < 0 || bpmDistanse > _distanse)
                {
                    bpmDistanse = _distanse;
                    bpmIndex = i;
                }
            }

            if (noteIndex[0] == -1 && noteIndex[1] == -1 && speedIndex == -1 && bpmIndex == -1)
            {
                return;
            }
                
            int check = 0;

            if (noteIndex[0] != -1 && noteIndex[1] != -1)
            {
                check = 1;
            }
            else if (speedIndex != -1)
            {
                check = 2;
            }
            else if (bpmIndex != -1)
            {
                check = 3;
            }
            
            if (check == 1 && speedIndex != -1 && speedDistanse <= noteDistanse)
            {
                check = 2;
            }

            if (check == 1 && bpmIndex != -1 && bpmDistanse <= noteDistanse)
            {
                check = 3;
            }

            if (check == 2 && bpmIndex != -1 && bpmDistanse <= speedDistanse)
            {
                check = 3;
            }
            
            if (check == 1)
            {
                SelectedSpeedChanged(-1);
                SelectedBPMChanged(-1);

                if (e.Button == MouseButtons.Right &&
                    selectedNoteIndex[0] == noteIndex[0] &&
                    selectedNoteIndex[1] == noteIndex[1])
                {
                    music.DeleteNote(hScrollBar_Difficulty.Value, noteIndex[0],
                                     currentNotes[1][noteIndex[0]][noteIndex[1]]);
                    PageChanged();
                }
                else
                {
                    SelectedNoteChanged(noteIndex[0], noteIndex[1]);
                }
            }
            else if (check == 2)
            {
                SelectedNoteChanged(-1, -1);
                SelectedBPMChanged(-1);

                if (e.Button == MouseButtons.Right &&
                    selectedSpeedIndex == speedIndex)
                {
                    music.DeleteSpeed(hScrollBar_Difficulty.Value, currentSpeeds[1][speedIndex]);
                    PageChanged();
                }
                else
                {
                    SelectedSpeedChanged(speedIndex);
                }
            }
            else if (check == 3)
            {
                SelectedNoteChanged(-1, -1);
                SelectedSpeedChanged(-1);

                if (e.Button == MouseButtons.Right &&
                    selectedBPMIndex == bpmIndex)
                {
                    music.DeleteBPM(hScrollBar_Difficulty.Value, currentBPMs[1][bpmIndex]);
                    PageChanged();
                }
                else
                {
                    SelectedBPMChanged(bpmIndex);
                }
            }
        }

        private void textBox_HitFile_MouseClick(object sender, MouseEventArgs e)
        {
            textBox_MusicFile.Text = GetFilePath("WAV (*.wav)|*.wav");
        }

        private void textBox_BPM_New_TextChanged(object sender, EventArgs e)
        {
            int value = -1;
            int.TryParse(textBox_BPM_New.Text, out value);

            if (value < 1)
            {
                value = 1;
            }

            textBox_BPM_New.Text = value.ToString();
        }

        private void button_BPM_Click(object sender, EventArgs e)
        {
            AddBPM();
        }

        private void timer_Serial_Tick(object sender, EventArgs e)
        {
            if (serialPort_Main.IsOpen)
            {
                try
                {
                    if (serialPort_Main.ReceivedBytesThreshold > 0)
                    {
                        datas += serialPort_Main.ReadExisting();

                        int lastIndex2 = datas.LastIndexOf('|');

                        if (lastIndex2 != -1)
                        {
                            int lastIndex = datas.LastIndexOf('!', lastIndex2 - 1);

                            if (lastIndex != -1 && lastIndex < lastIndex2)
                            {
                                currentData = datas.Substring(lastIndex + 1, lastIndex2 - (lastIndex + 1));

                                datas = datas.Substring(lastIndex2 + 1);
                            }
                        }
                    }
                }
                catch (Exception /*e*/)
                {
                    datas = "";
                }
            }
            else
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_SerialPort");

                foreach (ManagementObject managementObject in searcher.Get())
                {
                    if (managementObject["Caption"].ToString().Contains("Arduino Mega ADK"))
                    {
                        serialPort_Main.Close();
                        serialPort_Main.PortName = managementObject["DeviceID"].ToString();
                        serialPort_Main.Open();

                        //serialPort_Main.Write("!0 0 63 63 0 0 |");
                    }
                }
            }
        }
    }
}
