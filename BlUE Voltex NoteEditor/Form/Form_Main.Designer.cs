﻿namespace BlUE_Voltex_NoteEditor
{
    partial class Form_Main
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel_Previous = new System.Windows.Forms.Panel();
            this.panel_Current = new System.Windows.Forms.Panel();
            this.panel_Next = new System.Windows.Forms.Panel();
            this.panel_Menu = new System.Windows.Forms.Panel();
            this.tabControl_Menu = new System.Windows.Forms.TabControl();
            this.tabPage_Menu = new System.Windows.Forms.TabPage();
            this.label28 = new System.Windows.Forms.Label();
            this.textBox_HitFile = new System.Windows.Forms.TextBox();
            this.button_New = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.textBox_MusicFile = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox_ImageFile = new System.Windows.Forms.TextBox();
            this.textBox_SaveName = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.button_Create = new System.Windows.Forms.Button();
            this.button_InGamePlay = new System.Windows.Forms.Button();
            this.button_Load = new System.Windows.Forms.Button();
            this.button_Save = new System.Windows.Forms.Button();
            this.tabPage_Information = new System.Windows.Forms.TabPage();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox_MusicVolume = new System.Windows.Forms.TextBox();
            this.button_ReLoad = new System.Windows.Forms.Button();
            this.button_Set = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_HitVolume = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_PreviewPlayTime = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_PreviewStartTime = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_StartTime = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_StartBPM = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_Artist = new System.Windows.Forms.TextBox();
            this.textBox_Name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage_Note = new System.Windows.Forms.TabPage();
            this.button_BPM = new System.Windows.Forms.Button();
            this.textBox_BPM_Selected = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox_BPM_New = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.hScrollBar_Difficulty = new System.Windows.Forms.HScrollBar();
            this.button_LongNote = new System.Windows.Forms.Button();
            this.radioButton_HighToLow = new System.Windows.Forms.RadioButton();
            this.radioButton_LowToHigh = new System.Windows.Forms.RadioButton();
            this.radioButton_Normal = new System.Windows.Forms.RadioButton();
            this.textBox_PositionDenominator_Selected = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox_PositionDenominator_New = new System.Windows.Forms.TextBox();
            this.button_DragNote = new System.Windows.Forms.Button();
            this.button_Speed = new System.Windows.Forms.Button();
            this.button_RNote = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.button_LNote = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox_BarPerPage = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.button_Play = new System.Windows.Forms.Button();
            this.button_Stop = new System.Windows.Forms.Button();
            this.label_Page = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox_Page = new System.Windows.Forms.TextBox();
            this.hScrollBar_Page = new System.Windows.Forms.HScrollBar();
            this.textBox_Speed_Selected = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox_Speed_New = new System.Windows.Forms.TextBox();
            this.textBox_PositionNumerator_Selected = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox_PositionNumerator_New = new System.Windows.Forms.TextBox();
            this.textBox_BeatDenominator_Selected = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox_BeatDenominator_New = new System.Windows.Forms.TextBox();
            this.textBox_BeatNumerator_Selected = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox_BeatNumerator_New = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox_Order_Selected = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox_Order_New = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog_Load = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog_Load = new System.Windows.Forms.OpenFileDialog();
            this.timer_Main = new System.Windows.Forms.Timer(this.components);
            this.serialPort_Main = new System.IO.Ports.SerialPort(this.components);
            this.timer_Serial = new System.Windows.Forms.Timer(this.components);
            this.label30 = new System.Windows.Forms.Label();
            this.textBox_HardDifficulty = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox_NormalDifficulty = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.textBox_EasyDifficulty = new System.Windows.Forms.TextBox();
            this.panel_Menu.SuspendLayout();
            this.tabControl_Menu.SuspendLayout();
            this.tabPage_Menu.SuspendLayout();
            this.tabPage_Information.SuspendLayout();
            this.tabPage_Note.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_Previous
            // 
            this.panel_Previous.Location = new System.Drawing.Point(0, 0);
            this.panel_Previous.Margin = new System.Windows.Forms.Padding(0);
            this.panel_Previous.Name = "panel_Previous";
            this.panel_Previous.Size = new System.Drawing.Size(200, 600);
            this.panel_Previous.TabIndex = 3;
            this.panel_Previous.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_Previous_Paint);
            // 
            // panel_Current
            // 
            this.panel_Current.Location = new System.Drawing.Point(200, 0);
            this.panel_Current.Margin = new System.Windows.Forms.Padding(0);
            this.panel_Current.Name = "panel_Current";
            this.panel_Current.Size = new System.Drawing.Size(200, 600);
            this.panel_Current.TabIndex = 4;
            this.panel_Current.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_Current_Paint);
            this.panel_Current.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_Current_MouseClick);
            // 
            // panel_Next
            // 
            this.panel_Next.Location = new System.Drawing.Point(400, 0);
            this.panel_Next.Margin = new System.Windows.Forms.Padding(0);
            this.panel_Next.Name = "panel_Next";
            this.panel_Next.Size = new System.Drawing.Size(200, 600);
            this.panel_Next.TabIndex = 5;
            this.panel_Next.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_Next_Paint);
            // 
            // panel_Menu
            // 
            this.panel_Menu.Controls.Add(this.tabControl_Menu);
            this.panel_Menu.Location = new System.Drawing.Point(600, 0);
            this.panel_Menu.Margin = new System.Windows.Forms.Padding(0);
            this.panel_Menu.Name = "panel_Menu";
            this.panel_Menu.Size = new System.Drawing.Size(200, 600);
            this.panel_Menu.TabIndex = 6;
            // 
            // tabControl_Menu
            // 
            this.tabControl_Menu.Controls.Add(this.tabPage_Menu);
            this.tabControl_Menu.Controls.Add(this.tabPage_Information);
            this.tabControl_Menu.Controls.Add(this.tabPage_Note);
            this.tabControl_Menu.Location = new System.Drawing.Point(0, 0);
            this.tabControl_Menu.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl_Menu.Name = "tabControl_Menu";
            this.tabControl_Menu.Padding = new System.Drawing.Point(0, 0);
            this.tabControl_Menu.SelectedIndex = 0;
            this.tabControl_Menu.Size = new System.Drawing.Size(200, 600);
            this.tabControl_Menu.TabIndex = 50;
            // 
            // tabPage_Menu
            // 
            this.tabPage_Menu.Controls.Add(this.label28);
            this.tabPage_Menu.Controls.Add(this.textBox_HitFile);
            this.tabPage_Menu.Controls.Add(this.button_New);
            this.tabPage_Menu.Controls.Add(this.label24);
            this.tabPage_Menu.Controls.Add(this.textBox_MusicFile);
            this.tabPage_Menu.Controls.Add(this.label25);
            this.tabPage_Menu.Controls.Add(this.textBox_ImageFile);
            this.tabPage_Menu.Controls.Add(this.textBox_SaveName);
            this.tabPage_Menu.Controls.Add(this.label26);
            this.tabPage_Menu.Controls.Add(this.button_Create);
            this.tabPage_Menu.Controls.Add(this.button_InGamePlay);
            this.tabPage_Menu.Controls.Add(this.button_Load);
            this.tabPage_Menu.Controls.Add(this.button_Save);
            this.tabPage_Menu.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Menu.Name = "tabPage_Menu";
            this.tabPage_Menu.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Menu.Size = new System.Drawing.Size(192, 574);
            this.tabPage_Menu.TabIndex = 2;
            this.tabPage_Menu.Text = "Menu";
            this.tabPage_Menu.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 90);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(39, 12);
            this.label28.TabIndex = 100;
            this.label28.Text = "HitFile";
            // 
            // textBox_HitFile
            // 
            this.textBox_HitFile.Location = new System.Drawing.Point(86, 87);
            this.textBox_HitFile.Name = "textBox_HitFile";
            this.textBox_HitFile.Size = new System.Drawing.Size(100, 21);
            this.textBox_HitFile.TabIndex = 99;
            this.textBox_HitFile.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox_HitFile_MouseClick);
            // 
            // button_New
            // 
            this.button_New.Location = new System.Drawing.Point(6, 143);
            this.button_New.Name = "button_New";
            this.button_New.Size = new System.Drawing.Size(47, 23);
            this.button_New.TabIndex = 98;
            this.button_New.Text = "New";
            this.button_New.UseVisualStyleBackColor = true;
            this.button_New.Click += new System.EventHandler(this.button_New_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 63);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(60, 12);
            this.label24.TabIndex = 97;
            this.label24.Text = "MusicFile";
            // 
            // textBox_MusicFile
            // 
            this.textBox_MusicFile.Location = new System.Drawing.Point(86, 60);
            this.textBox_MusicFile.Name = "textBox_MusicFile";
            this.textBox_MusicFile.Size = new System.Drawing.Size(100, 21);
            this.textBox_MusicFile.TabIndex = 96;
            this.textBox_MusicFile.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox_MusicFile_MouseClick);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 36);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(60, 12);
            this.label25.TabIndex = 95;
            this.label25.Text = "ImageFile";
            // 
            // textBox_ImageFile
            // 
            this.textBox_ImageFile.Location = new System.Drawing.Point(86, 33);
            this.textBox_ImageFile.Name = "textBox_ImageFile";
            this.textBox_ImageFile.Size = new System.Drawing.Size(100, 21);
            this.textBox_ImageFile.TabIndex = 94;
            this.textBox_ImageFile.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox_ImageFile_MouseClick);
            // 
            // textBox_SaveName
            // 
            this.textBox_SaveName.Location = new System.Drawing.Point(86, 6);
            this.textBox_SaveName.Name = "textBox_SaveName";
            this.textBox_SaveName.Size = new System.Drawing.Size(100, 21);
            this.textBox_SaveName.TabIndex = 93;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 9);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(67, 12);
            this.label26.TabIndex = 92;
            this.label26.Text = "SaveName";
            // 
            // button_Create
            // 
            this.button_Create.Location = new System.Drawing.Point(86, 114);
            this.button_Create.Name = "button_Create";
            this.button_Create.Size = new System.Drawing.Size(100, 23);
            this.button_Create.TabIndex = 91;
            this.button_Create.Text = "Create";
            this.button_Create.UseVisualStyleBackColor = true;
            this.button_Create.Click += new System.EventHandler(this.button_Create_Click);
            // 
            // button_InGamePlay
            // 
            this.button_InGamePlay.Enabled = false;
            this.button_InGamePlay.Location = new System.Drawing.Point(86, 172);
            this.button_InGamePlay.Name = "button_InGamePlay";
            this.button_InGamePlay.Size = new System.Drawing.Size(100, 23);
            this.button_InGamePlay.TabIndex = 90;
            this.button_InGamePlay.Text = "In Game Play";
            this.button_InGamePlay.UseVisualStyleBackColor = true;
            this.button_InGamePlay.Click += new System.EventHandler(this.button_InGamePlay_Click);
            // 
            // button_Load
            // 
            this.button_Load.Location = new System.Drawing.Point(86, 143);
            this.button_Load.Name = "button_Load";
            this.button_Load.Size = new System.Drawing.Size(47, 23);
            this.button_Load.TabIndex = 89;
            this.button_Load.Text = "Load";
            this.button_Load.UseVisualStyleBackColor = true;
            this.button_Load.Click += new System.EventHandler(this.button_Load_Click);
            // 
            // button_Save
            // 
            this.button_Save.Enabled = false;
            this.button_Save.Location = new System.Drawing.Point(139, 143);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(47, 23);
            this.button_Save.TabIndex = 88;
            this.button_Save.Text = "Save";
            this.button_Save.UseVisualStyleBackColor = true;
            this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // tabPage_Information
            // 
            this.tabPage_Information.Controls.Add(this.textBox_EasyDifficulty);
            this.tabPage_Information.Controls.Add(this.label32);
            this.tabPage_Information.Controls.Add(this.label31);
            this.tabPage_Information.Controls.Add(this.textBox_NormalDifficulty);
            this.tabPage_Information.Controls.Add(this.label30);
            this.tabPage_Information.Controls.Add(this.textBox_HardDifficulty);
            this.tabPage_Information.Controls.Add(this.label27);
            this.tabPage_Information.Controls.Add(this.textBox_MusicVolume);
            this.tabPage_Information.Controls.Add(this.button_ReLoad);
            this.tabPage_Information.Controls.Add(this.button_Set);
            this.tabPage_Information.Controls.Add(this.label7);
            this.tabPage_Information.Controls.Add(this.textBox_HitVolume);
            this.tabPage_Information.Controls.Add(this.label6);
            this.tabPage_Information.Controls.Add(this.textBox_PreviewPlayTime);
            this.tabPage_Information.Controls.Add(this.label5);
            this.tabPage_Information.Controls.Add(this.textBox_PreviewStartTime);
            this.tabPage_Information.Controls.Add(this.label4);
            this.tabPage_Information.Controls.Add(this.textBox_StartTime);
            this.tabPage_Information.Controls.Add(this.label3);
            this.tabPage_Information.Controls.Add(this.textBox_StartBPM);
            this.tabPage_Information.Controls.Add(this.label2);
            this.tabPage_Information.Controls.Add(this.textBox_Artist);
            this.tabPage_Information.Controls.Add(this.textBox_Name);
            this.tabPage_Information.Controls.Add(this.label1);
            this.tabPage_Information.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Information.Name = "tabPage_Information";
            this.tabPage_Information.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Information.Size = new System.Drawing.Size(192, 574);
            this.tabPage_Information.TabIndex = 0;
            this.tabPage_Information.Text = "Information";
            this.tabPage_Information.UseVisualStyleBackColor = true;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 207);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(83, 12);
            this.label27.TabIndex = 95;
            this.label27.Text = "MusicVolume";
            // 
            // textBox_MusicVolume
            // 
            this.textBox_MusicVolume.Location = new System.Drawing.Point(86, 222);
            this.textBox_MusicVolume.Name = "textBox_MusicVolume";
            this.textBox_MusicVolume.Size = new System.Drawing.Size(100, 21);
            this.textBox_MusicVolume.TabIndex = 94;
            // 
            // button_ReLoad
            // 
            this.button_ReLoad.Location = new System.Drawing.Point(86, 384);
            this.button_ReLoad.Name = "button_ReLoad";
            this.button_ReLoad.Size = new System.Drawing.Size(100, 23);
            this.button_ReLoad.TabIndex = 93;
            this.button_ReLoad.Text = "Re Load";
            this.button_ReLoad.UseVisualStyleBackColor = true;
            this.button_ReLoad.Click += new System.EventHandler(this.button_ReLoad_Click);
            // 
            // button_Set
            // 
            this.button_Set.Location = new System.Drawing.Point(86, 413);
            this.button_Set.Name = "button_Set";
            this.button_Set.Size = new System.Drawing.Size(100, 23);
            this.button_Set.TabIndex = 92;
            this.button_Set.Text = "Set (Save)";
            this.button_Set.UseVisualStyleBackColor = true;
            this.button_Set.Click += new System.EventHandler(this.button_Set_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 252);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 12);
            this.label7.TabIndex = 61;
            this.label7.Text = "HitVolume";
            // 
            // textBox_HitVolume
            // 
            this.textBox_HitVolume.Location = new System.Drawing.Point(86, 249);
            this.textBox_HitVolume.Name = "textBox_HitVolume";
            this.textBox_HitVolume.Size = new System.Drawing.Size(100, 21);
            this.textBox_HitVolume.TabIndex = 60;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 12);
            this.label6.TabIndex = 59;
            this.label6.Text = "PreviewPlayTime";
            // 
            // textBox_PreviewPlayTime
            // 
            this.textBox_PreviewPlayTime.Location = new System.Drawing.Point(86, 177);
            this.textBox_PreviewPlayTime.Name = "textBox_PreviewPlayTime";
            this.textBox_PreviewPlayTime.Size = new System.Drawing.Size(100, 21);
            this.textBox_PreviewPlayTime.TabIndex = 58;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 12);
            this.label5.TabIndex = 57;
            this.label5.Text = "PreviewStartTime";
            // 
            // textBox_PreviewStartTime
            // 
            this.textBox_PreviewStartTime.Location = new System.Drawing.Point(86, 132);
            this.textBox_PreviewStartTime.Name = "textBox_PreviewStartTime";
            this.textBox_PreviewStartTime.Size = new System.Drawing.Size(100, 21);
            this.textBox_PreviewStartTime.TabIndex = 56;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 55;
            this.label4.Text = "StartTime";
            // 
            // textBox_StartTime
            // 
            this.textBox_StartTime.Location = new System.Drawing.Point(86, 87);
            this.textBox_StartTime.Name = "textBox_StartTime";
            this.textBox_StartTime.Size = new System.Drawing.Size(100, 21);
            this.textBox_StartTime.TabIndex = 54;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 12);
            this.label3.TabIndex = 53;
            this.label3.Text = "StartBPM";
            // 
            // textBox_StartBPM
            // 
            this.textBox_StartBPM.Location = new System.Drawing.Point(86, 60);
            this.textBox_StartBPM.Name = "textBox_StartBPM";
            this.textBox_StartBPM.Size = new System.Drawing.Size(100, 21);
            this.textBox_StartBPM.TabIndex = 52;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 12);
            this.label2.TabIndex = 51;
            this.label2.Text = "Artist";
            // 
            // textBox_Artist
            // 
            this.textBox_Artist.Location = new System.Drawing.Point(86, 33);
            this.textBox_Artist.Name = "textBox_Artist";
            this.textBox_Artist.Size = new System.Drawing.Size(100, 21);
            this.textBox_Artist.TabIndex = 50;
            // 
            // textBox_Name
            // 
            this.textBox_Name.Location = new System.Drawing.Point(86, 6);
            this.textBox_Name.Name = "textBox_Name";
            this.textBox_Name.Size = new System.Drawing.Size(100, 21);
            this.textBox_Name.TabIndex = 49;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 12);
            this.label1.TabIndex = 48;
            this.label1.Text = "Name";
            // 
            // tabPage_Note
            // 
            this.tabPage_Note.Controls.Add(this.button_BPM);
            this.tabPage_Note.Controls.Add(this.textBox_BPM_Selected);
            this.tabPage_Note.Controls.Add(this.label29);
            this.tabPage_Note.Controls.Add(this.textBox_BPM_New);
            this.tabPage_Note.Controls.Add(this.label20);
            this.tabPage_Note.Controls.Add(this.label22);
            this.tabPage_Note.Controls.Add(this.hScrollBar_Difficulty);
            this.tabPage_Note.Controls.Add(this.button_LongNote);
            this.tabPage_Note.Controls.Add(this.radioButton_HighToLow);
            this.tabPage_Note.Controls.Add(this.radioButton_LowToHigh);
            this.tabPage_Note.Controls.Add(this.radioButton_Normal);
            this.tabPage_Note.Controls.Add(this.textBox_PositionDenominator_Selected);
            this.tabPage_Note.Controls.Add(this.label23);
            this.tabPage_Note.Controls.Add(this.textBox_PositionDenominator_New);
            this.tabPage_Note.Controls.Add(this.button_DragNote);
            this.tabPage_Note.Controls.Add(this.button_Speed);
            this.tabPage_Note.Controls.Add(this.button_RNote);
            this.tabPage_Note.Controls.Add(this.label21);
            this.tabPage_Note.Controls.Add(this.button_LNote);
            this.tabPage_Note.Controls.Add(this.label18);
            this.tabPage_Note.Controls.Add(this.label19);
            this.tabPage_Note.Controls.Add(this.textBox_BarPerPage);
            this.tabPage_Note.Controls.Add(this.label17);
            this.tabPage_Note.Controls.Add(this.button_Play);
            this.tabPage_Note.Controls.Add(this.button_Stop);
            this.tabPage_Note.Controls.Add(this.label_Page);
            this.tabPage_Note.Controls.Add(this.label16);
            this.tabPage_Note.Controls.Add(this.label15);
            this.tabPage_Note.Controls.Add(this.textBox_Page);
            this.tabPage_Note.Controls.Add(this.hScrollBar_Page);
            this.tabPage_Note.Controls.Add(this.textBox_Speed_Selected);
            this.tabPage_Note.Controls.Add(this.label14);
            this.tabPage_Note.Controls.Add(this.textBox_Speed_New);
            this.tabPage_Note.Controls.Add(this.textBox_PositionNumerator_Selected);
            this.tabPage_Note.Controls.Add(this.label13);
            this.tabPage_Note.Controls.Add(this.textBox_PositionNumerator_New);
            this.tabPage_Note.Controls.Add(this.textBox_BeatDenominator_Selected);
            this.tabPage_Note.Controls.Add(this.label12);
            this.tabPage_Note.Controls.Add(this.textBox_BeatDenominator_New);
            this.tabPage_Note.Controls.Add(this.textBox_BeatNumerator_Selected);
            this.tabPage_Note.Controls.Add(this.label11);
            this.tabPage_Note.Controls.Add(this.textBox_BeatNumerator_New);
            this.tabPage_Note.Controls.Add(this.label10);
            this.tabPage_Note.Controls.Add(this.label8);
            this.tabPage_Note.Controls.Add(this.textBox_Order_Selected);
            this.tabPage_Note.Controls.Add(this.label9);
            this.tabPage_Note.Controls.Add(this.textBox_Order_New);
            this.tabPage_Note.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Note.Name = "tabPage_Note";
            this.tabPage_Note.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Note.Size = new System.Drawing.Size(192, 574);
            this.tabPage_Note.TabIndex = 1;
            this.tabPage_Note.Text = "Note";
            this.tabPage_Note.UseVisualStyleBackColor = true;
            // 
            // button_BPM
            // 
            this.button_BPM.Location = new System.Drawing.Point(109, 333);
            this.button_BPM.Name = "button_BPM";
            this.button_BPM.Size = new System.Drawing.Size(75, 23);
            this.button_BPM.TabIndex = 117;
            this.button_BPM.Text = "BPM";
            this.button_BPM.UseVisualStyleBackColor = true;
            this.button_BPM.Click += new System.EventHandler(this.button_BPM_Click);
            // 
            // textBox_BPM_Selected
            // 
            this.textBox_BPM_Selected.Location = new System.Drawing.Point(86, 252);
            this.textBox_BPM_Selected.Name = "textBox_BPM_Selected";
            this.textBox_BPM_Selected.ReadOnly = true;
            this.textBox_BPM_Selected.Size = new System.Drawing.Size(47, 21);
            this.textBox_BPM_Selected.TabIndex = 116;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 255);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(32, 12);
            this.label29.TabIndex = 115;
            this.label29.Text = "BPM";
            // 
            // textBox_BPM_New
            // 
            this.textBox_BPM_New.Location = new System.Drawing.Point(139, 252);
            this.textBox_BPM_New.Name = "textBox_BPM_New";
            this.textBox_BPM_New.Size = new System.Drawing.Size(47, 21);
            this.textBox_BPM_New.TabIndex = 114;
            this.textBox_BPM_New.TextChanged += new System.EventHandler(this.textBox_BPM_New_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 371);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 12);
            this.label20.TabIndex = 113;
            this.label20.Text = "Selected";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 452);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(52, 12);
            this.label22.TabIndex = 112;
            this.label22.Text = "Difficulty";
            // 
            // hScrollBar_Difficulty
            // 
            this.hScrollBar_Difficulty.LargeChange = 1;
            this.hScrollBar_Difficulty.Location = new System.Drawing.Point(86, 450);
            this.hScrollBar_Difficulty.Maximum = 2;
            this.hScrollBar_Difficulty.Name = "hScrollBar_Difficulty";
            this.hScrollBar_Difficulty.Size = new System.Drawing.Size(103, 17);
            this.hScrollBar_Difficulty.TabIndex = 111;
            this.hScrollBar_Difficulty.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar_Difficulty_Scroll);
            // 
            // button_LongNote
            // 
            this.button_LongNote.Location = new System.Drawing.Point(6, 386);
            this.button_LongNote.Name = "button_LongNote";
            this.button_LongNote.Size = new System.Drawing.Size(75, 23);
            this.button_LongNote.TabIndex = 110;
            this.button_LongNote.Text = "Long Note";
            this.button_LongNote.UseVisualStyleBackColor = true;
            this.button_LongNote.Click += new System.EventHandler(this.button_LongNote_Click);
            // 
            // radioButton_HighToLow
            // 
            this.radioButton_HighToLow.AutoSize = true;
            this.radioButton_HighToLow.Location = new System.Drawing.Point(157, 418);
            this.radioButton_HighToLow.Name = "radioButton_HighToLow";
            this.radioButton_HighToLow.Size = new System.Drawing.Size(31, 16);
            this.radioButton_HighToLow.TabIndex = 109;
            this.radioButton_HighToLow.Text = "∩";
            this.radioButton_HighToLow.UseVisualStyleBackColor = true;
            // 
            // radioButton_LowToHigh
            // 
            this.radioButton_LowToHigh.AutoSize = true;
            this.radioButton_LowToHigh.Location = new System.Drawing.Point(122, 418);
            this.radioButton_LowToHigh.Name = "radioButton_LowToHigh";
            this.radioButton_LowToHigh.Size = new System.Drawing.Size(35, 16);
            this.radioButton_LowToHigh.TabIndex = 108;
            this.radioButton_LowToHigh.Text = "∪";
            this.radioButton_LowToHigh.UseVisualStyleBackColor = true;
            // 
            // radioButton_Normal
            // 
            this.radioButton_Normal.AutoSize = true;
            this.radioButton_Normal.Checked = true;
            this.radioButton_Normal.Location = new System.Drawing.Point(87, 418);
            this.radioButton_Normal.Name = "radioButton_Normal";
            this.radioButton_Normal.Size = new System.Drawing.Size(35, 16);
            this.radioButton_Normal.TabIndex = 107;
            this.radioButton_Normal.TabStop = true;
            this.radioButton_Normal.Text = "／";
            this.radioButton_Normal.UseVisualStyleBackColor = true;
            // 
            // textBox_PositionDenominator_Selected
            // 
            this.textBox_PositionDenominator_Selected.Location = new System.Drawing.Point(86, 198);
            this.textBox_PositionDenominator_Selected.Name = "textBox_PositionDenominator_Selected";
            this.textBox_PositionDenominator_Selected.ReadOnly = true;
            this.textBox_PositionDenominator_Selected.Size = new System.Drawing.Size(47, 21);
            this.textBox_PositionDenominator_Selected.TabIndex = 106;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 183);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(121, 12);
            this.label23.TabIndex = 105;
            this.label23.Text = "PositionDenominator";
            // 
            // textBox_PositionDenominator_New
            // 
            this.textBox_PositionDenominator_New.Location = new System.Drawing.Point(139, 198);
            this.textBox_PositionDenominator_New.Name = "textBox_PositionDenominator_New";
            this.textBox_PositionDenominator_New.Size = new System.Drawing.Size(47, 21);
            this.textBox_PositionDenominator_New.TabIndex = 104;
            this.textBox_PositionDenominator_New.TextChanged += new System.EventHandler(this.textBox_PositionDenominator_New_TextChanged);
            // 
            // button_DragNote
            // 
            this.button_DragNote.Location = new System.Drawing.Point(6, 415);
            this.button_DragNote.Name = "button_DragNote";
            this.button_DragNote.Size = new System.Drawing.Size(75, 23);
            this.button_DragNote.TabIndex = 101;
            this.button_DragNote.Text = "Drag Note";
            this.button_DragNote.UseVisualStyleBackColor = true;
            this.button_DragNote.Click += new System.EventHandler(this.button_DragNote_Click);
            // 
            // button_Speed
            // 
            this.button_Speed.Location = new System.Drawing.Point(6, 333);
            this.button_Speed.Name = "button_Speed";
            this.button_Speed.Size = new System.Drawing.Size(75, 23);
            this.button_Speed.TabIndex = 97;
            this.button_Speed.Text = "Speed";
            this.button_Speed.UseVisualStyleBackColor = true;
            this.button_Speed.Click += new System.EventHandler(this.button_Speed_Click);
            // 
            // button_RNote
            // 
            this.button_RNote.Location = new System.Drawing.Point(109, 304);
            this.button_RNote.Name = "button_RNote";
            this.button_RNote.Size = new System.Drawing.Size(75, 23);
            this.button_RNote.TabIndex = 95;
            this.button_RNote.Text = "R Note";
            this.button_RNote.UseVisualStyleBackColor = true;
            this.button_RNote.Click += new System.EventHandler(this.button_RNote_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 289);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(31, 12);
            this.label21.TabIndex = 94;
            this.label21.Text = "New";
            // 
            // button_LNote
            // 
            this.button_LNote.Location = new System.Drawing.Point(6, 304);
            this.button_LNote.Name = "button_LNote";
            this.button_LNote.Size = new System.Drawing.Size(75, 23);
            this.button_LNote.TabIndex = 91;
            this.button_LNote.Text = "L Note";
            this.button_LNote.UseVisualStyleBackColor = true;
            this.button_LNote.Click += new System.EventHandler(this.button_LNote_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(156, 473);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(11, 12);
            this.label18.TabIndex = 89;
            this.label18.Text = "4";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(139, 473);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(11, 12);
            this.label19.TabIndex = 88;
            this.label19.Text = "/";
            // 
            // textBox_BarPerPage
            // 
            this.textBox_BarPerPage.Location = new System.Drawing.Point(86, 470);
            this.textBox_BarPerPage.Name = "textBox_BarPerPage";
            this.textBox_BarPerPage.Size = new System.Drawing.Size(47, 21);
            this.textBox_BarPerPage.TabIndex = 87;
            this.textBox_BarPerPage.Text = "4";
            this.textBox_BarPerPage.TextChanged += new System.EventHandler(this.textBox_BarPerPage_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 473);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 12);
            this.label17.TabIndex = 86;
            this.label17.Text = "BarPerPage";
            // 
            // button_Play
            // 
            this.button_Play.Location = new System.Drawing.Point(6, 543);
            this.button_Play.Name = "button_Play";
            this.button_Play.Size = new System.Drawing.Size(80, 23);
            this.button_Play.TabIndex = 85;
            this.button_Play.Text = "Play";
            this.button_Play.UseVisualStyleBackColor = true;
            this.button_Play.Click += new System.EventHandler(this.button_Play_Click);
            // 
            // button_Stop
            // 
            this.button_Stop.Location = new System.Drawing.Point(111, 543);
            this.button_Stop.Name = "button_Stop";
            this.button_Stop.Size = new System.Drawing.Size(75, 23);
            this.button_Stop.TabIndex = 84;
            this.button_Stop.Text = "Stop";
            this.button_Stop.UseVisualStyleBackColor = true;
            this.button_Stop.Click += new System.EventHandler(this.button_Stop_Click);
            // 
            // label_Page
            // 
            this.label_Page.AutoSize = true;
            this.label_Page.Location = new System.Drawing.Point(156, 500);
            this.label_Page.Name = "label_Page";
            this.label_Page.Size = new System.Drawing.Size(11, 12);
            this.label_Page.TabIndex = 83;
            this.label_Page.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(139, 500);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(11, 12);
            this.label16.TabIndex = 82;
            this.label16.Text = "/";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 500);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 12);
            this.label15.TabIndex = 81;
            this.label15.Text = "Page";
            // 
            // textBox_Page
            // 
            this.textBox_Page.Location = new System.Drawing.Point(86, 497);
            this.textBox_Page.Name = "textBox_Page";
            this.textBox_Page.Size = new System.Drawing.Size(47, 21);
            this.textBox_Page.TabIndex = 80;
            this.textBox_Page.Text = "1";
            this.textBox_Page.TextChanged += new System.EventHandler(this.textBox_Page_TextChanged);
            // 
            // hScrollBar_Page
            // 
            this.hScrollBar_Page.LargeChange = 1;
            this.hScrollBar_Page.Location = new System.Drawing.Point(3, 521);
            this.hScrollBar_Page.Maximum = 1;
            this.hScrollBar_Page.Minimum = 1;
            this.hScrollBar_Page.Name = "hScrollBar_Page";
            this.hScrollBar_Page.Size = new System.Drawing.Size(186, 17);
            this.hScrollBar_Page.TabIndex = 79;
            this.hScrollBar_Page.Value = 1;
            this.hScrollBar_Page.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar_Page_Scroll);
            // 
            // textBox_Speed_Selected
            // 
            this.textBox_Speed_Selected.Location = new System.Drawing.Point(86, 225);
            this.textBox_Speed_Selected.Name = "textBox_Speed_Selected";
            this.textBox_Speed_Selected.ReadOnly = true;
            this.textBox_Speed_Selected.Size = new System.Drawing.Size(47, 21);
            this.textBox_Speed_Selected.TabIndex = 64;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 228);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 12);
            this.label14.TabIndex = 63;
            this.label14.Text = "Speed";
            // 
            // textBox_Speed_New
            // 
            this.textBox_Speed_New.Location = new System.Drawing.Point(139, 225);
            this.textBox_Speed_New.Name = "textBox_Speed_New";
            this.textBox_Speed_New.Size = new System.Drawing.Size(47, 21);
            this.textBox_Speed_New.TabIndex = 62;
            this.textBox_Speed_New.TextChanged += new System.EventHandler(this.textBox_Speed_New_TextChanged);
            // 
            // textBox_PositionNumerator_Selected
            // 
            this.textBox_PositionNumerator_Selected.Location = new System.Drawing.Point(86, 153);
            this.textBox_PositionNumerator_Selected.Name = "textBox_PositionNumerator_Selected";
            this.textBox_PositionNumerator_Selected.ReadOnly = true;
            this.textBox_PositionNumerator_Selected.Size = new System.Drawing.Size(47, 21);
            this.textBox_PositionNumerator_Selected.TabIndex = 61;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 138);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(113, 12);
            this.label13.TabIndex = 60;
            this.label13.Text = "PosotionNumerator";
            // 
            // textBox_PositionNumerator_New
            // 
            this.textBox_PositionNumerator_New.Location = new System.Drawing.Point(139, 153);
            this.textBox_PositionNumerator_New.Name = "textBox_PositionNumerator_New";
            this.textBox_PositionNumerator_New.Size = new System.Drawing.Size(47, 21);
            this.textBox_PositionNumerator_New.TabIndex = 59;
            this.textBox_PositionNumerator_New.TextChanged += new System.EventHandler(this.textBox_PositionNumerator_New_TextChanged);
            // 
            // textBox_BeatDenominator_Selected
            // 
            this.textBox_BeatDenominator_Selected.Location = new System.Drawing.Point(86, 108);
            this.textBox_BeatDenominator_Selected.Name = "textBox_BeatDenominator_Selected";
            this.textBox_BeatDenominator_Selected.ReadOnly = true;
            this.textBox_BeatDenominator_Selected.Size = new System.Drawing.Size(47, 21);
            this.textBox_BeatDenominator_Selected.TabIndex = 58;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 93);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(101, 12);
            this.label12.TabIndex = 57;
            this.label12.Text = "BeatDenominator";
            // 
            // textBox_BeatDenominator_New
            // 
            this.textBox_BeatDenominator_New.Location = new System.Drawing.Point(139, 108);
            this.textBox_BeatDenominator_New.Name = "textBox_BeatDenominator_New";
            this.textBox_BeatDenominator_New.Size = new System.Drawing.Size(47, 21);
            this.textBox_BeatDenominator_New.TabIndex = 56;
            this.textBox_BeatDenominator_New.TextChanged += new System.EventHandler(this.textBox_BeatDenominator_New_TextChanged);
            // 
            // textBox_BeatNumerator_Selected
            // 
            this.textBox_BeatNumerator_Selected.Location = new System.Drawing.Point(86, 63);
            this.textBox_BeatNumerator_Selected.Name = "textBox_BeatNumerator_Selected";
            this.textBox_BeatNumerator_Selected.ReadOnly = true;
            this.textBox_BeatNumerator_Selected.Size = new System.Drawing.Size(47, 21);
            this.textBox_BeatNumerator_Selected.TabIndex = 55;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 12);
            this.label11.TabIndex = 54;
            this.label11.Text = "BeatNumerator";
            // 
            // textBox_BeatNumerator_New
            // 
            this.textBox_BeatNumerator_New.Location = new System.Drawing.Point(139, 63);
            this.textBox_BeatNumerator_New.Name = "textBox_BeatNumerator_New";
            this.textBox_BeatNumerator_New.Size = new System.Drawing.Size(47, 21);
            this.textBox_BeatNumerator_New.TabIndex = 53;
            this.textBox_BeatNumerator_New.TextChanged += new System.EventHandler(this.textBox_BeatNumerator_New_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(137, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 12);
            this.label10.TabIndex = 52;
            this.label10.Text = "New";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(84, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 12);
            this.label8.TabIndex = 51;
            this.label8.Text = "Selected";
            // 
            // textBox_Order_Selected
            // 
            this.textBox_Order_Selected.Location = new System.Drawing.Point(86, 18);
            this.textBox_Order_Selected.Name = "textBox_Order_Selected";
            this.textBox_Order_Selected.ReadOnly = true;
            this.textBox_Order_Selected.Size = new System.Drawing.Size(47, 21);
            this.textBox_Order_Selected.TabIndex = 50;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 12);
            this.label9.TabIndex = 49;
            this.label9.Text = "Order";
            // 
            // textBox_Order_New
            // 
            this.textBox_Order_New.Location = new System.Drawing.Point(139, 18);
            this.textBox_Order_New.Name = "textBox_Order_New";
            this.textBox_Order_New.Size = new System.Drawing.Size(47, 21);
            this.textBox_Order_New.TabIndex = 48;
            this.textBox_Order_New.TextChanged += new System.EventHandler(this.textBox_Order_New_TextChanged);
            // 
            // timer_Main
            // 
            this.timer_Main.Interval = 1;
            this.timer_Main.Tick += new System.EventHandler(this.timer_Main_Tick);
            // 
            // timer_Serial
            // 
            this.timer_Serial.Enabled = true;
            this.timer_Serial.Interval = 1;
            this.timer_Serial.Tick += new System.EventHandler(this.timer_Serial_Tick);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 279);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(78, 12);
            this.label30.TabIndex = 97;
            this.label30.Text = "HardDifficulty";
            // 
            // textBox_HardDifficulty
            // 
            this.textBox_HardDifficulty.Location = new System.Drawing.Point(86, 276);
            this.textBox_HardDifficulty.Name = "textBox_HardDifficulty";
            this.textBox_HardDifficulty.Size = new System.Drawing.Size(100, 21);
            this.textBox_HardDifficulty.TabIndex = 96;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 306);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(93, 12);
            this.label31.TabIndex = 99;
            this.label31.Text = "NormalDifficulty";
            // 
            // textBox_NormalDifficulty
            // 
            this.textBox_NormalDifficulty.Location = new System.Drawing.Point(86, 330);
            this.textBox_NormalDifficulty.Name = "textBox_NormalDifficulty";
            this.textBox_NormalDifficulty.Size = new System.Drawing.Size(100, 21);
            this.textBox_NormalDifficulty.TabIndex = 98;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 360);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(81, 12);
            this.label32.TabIndex = 101;
            this.label32.Text = "EasyDifficulty";
            // 
            // textBox_EasyDifficulty
            // 
            this.textBox_EasyDifficulty.Location = new System.Drawing.Point(86, 357);
            this.textBox_EasyDifficulty.Name = "textBox_EasyDifficulty";
            this.textBox_EasyDifficulty.Size = new System.Drawing.Size(100, 21);
            this.textBox_EasyDifficulty.TabIndex = 102;
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.panel_Menu);
            this.Controls.Add(this.panel_Next);
            this.Controls.Add(this.panel_Current);
            this.Controls.Add(this.panel_Previous);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "Form_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BlUE Voltex NoteEditor";
            this.panel_Menu.ResumeLayout(false);
            this.tabControl_Menu.ResumeLayout(false);
            this.tabPage_Menu.ResumeLayout(false);
            this.tabPage_Menu.PerformLayout();
            this.tabPage_Information.ResumeLayout(false);
            this.tabPage_Information.PerformLayout();
            this.tabPage_Note.ResumeLayout(false);
            this.tabPage_Note.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_Previous;
        private System.Windows.Forms.Panel panel_Current;
        private System.Windows.Forms.Panel panel_Next;
        private System.Windows.Forms.Panel panel_Menu;
        private System.Windows.Forms.TabControl tabControl_Menu;
        private System.Windows.Forms.TabPage tabPage_Information;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_HitVolume;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_PreviewPlayTime;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_PreviewStartTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_StartTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_StartBPM;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_Artist;
        private System.Windows.Forms.TextBox textBox_Name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage_Note;
        private System.Windows.Forms.Button button_Speed;
        private System.Windows.Forms.Button button_RNote;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button button_LNote;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox_BarPerPage;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button_Play;
        private System.Windows.Forms.Button button_Stop;
        private System.Windows.Forms.Label label_Page;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox_Page;
        private System.Windows.Forms.HScrollBar hScrollBar_Page;
        private System.Windows.Forms.TextBox textBox_Speed_Selected;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox_Speed_New;
        private System.Windows.Forms.TextBox textBox_PositionNumerator_Selected;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox_PositionNumerator_New;
        private System.Windows.Forms.TextBox textBox_BeatDenominator_Selected;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox_BeatDenominator_New;
        private System.Windows.Forms.TextBox textBox_BeatNumerator_Selected;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox_BeatNumerator_New;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox_Order_Selected;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox_Order_New;
        private System.Windows.Forms.Button button_DragNote;
        private System.Windows.Forms.TextBox textBox_PositionDenominator_Selected;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox_PositionDenominator_New;
        private System.Windows.Forms.RadioButton radioButton_HighToLow;
        private System.Windows.Forms.RadioButton radioButton_LowToHigh;
        private System.Windows.Forms.RadioButton radioButton_Normal;
        private System.Windows.Forms.Button button_LongNote;
        private System.Windows.Forms.TabPage tabPage_Menu;
        private System.Windows.Forms.Button button_Create;
        private System.Windows.Forms.Button button_InGamePlay;
        private System.Windows.Forms.Button button_Load;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBox_MusicFile;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox_ImageFile;
        private System.Windows.Forms.TextBox textBox_SaveName;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog_Load;
        private System.Windows.Forms.OpenFileDialog openFileDialog_Load;
        private System.Windows.Forms.Button button_New;
        private System.Windows.Forms.Button button_Set;
        private System.Windows.Forms.Button button_ReLoad;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.HScrollBar hScrollBar_Difficulty;
        private System.Windows.Forms.Timer timer_Main;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBox_MusicVolume;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBox_HitFile;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox_BPM_Selected;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox_BPM_New;
        private System.Windows.Forms.Button button_BPM;
        private System.IO.Ports.SerialPort serialPort_Main;
        private System.Windows.Forms.Timer timer_Serial;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox_NormalDifficulty;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox_HardDifficulty;
        private System.Windows.Forms.TextBox textBox_EasyDifficulty;

    }
}

