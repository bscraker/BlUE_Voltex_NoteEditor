﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlUE_Voltex_NoteEditor.Class
{
    class Information
    {
        public String   name                = "";
        public String   artist              = "";
        public float    startBPM            = 0.0f;
        public int      startTime           = 0;
        public int      previewStartTime    = 0;
        public int      previewPlayTime     = 0;
        public float    musicVolume         = 1.0f;
        public float    hitVolume           = 1.0f;
        public int      hardDifficulty      = 0;
        public int      normalDifficulty    = 0;
        public int      easyDifficulty      = 0;
    }
}
