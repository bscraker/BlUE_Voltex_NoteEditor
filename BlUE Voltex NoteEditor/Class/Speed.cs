﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlUE_Voltex_NoteEditor.Class
{
    class Speed
    {
        public int      order           = 0;
        public int      beatNumerator   = 0;
        public int      beatDenominator = 1;
        public float    speed           = 1.0f;
    }
}
